import java.util*;



private Map <Integer, Integer> memo = new HashMap<>();


public static int fib(int n){

    if ( n < 0){
        throw new IllegalArgumentException("Indes is negative doesn't work");
    }


    if (n ==  0 || n == 1){
        return n;
    }

    // see if we already calculated thi
    if(memo.containsKey(n)){
        System.out.printf("grabbing memo[%d]\n", n);
        return memo.get(n);
    }

    System.out.printf("Comoputing fib(%d)\n" , n);
    int result = fib(n - 1 ) + fib(n -2);


    // memoize
    memo.put(n , result);

    return result;
}