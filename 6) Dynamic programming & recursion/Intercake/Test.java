import java.io.*;
class Test {
    public static int makeChange(int[] coins, int money, int recursionLevel) {
        //[5 , 10, 25]
        if (money == 0) {
            return 1;
        }
        if (money < 0) {
            return 0;
        }

        String indentation = "   ".repeat(recursionLevel);

        int numSubProblemWays = 0;
        for (int i = 0; i < coins.length; i++) {
            int remaining = money - coins[i];
            System.out.println(indentation + "inside call to makeChange(coins," + money + " - " + coins[i] + " = " + remaining + " remaining. calling makeChange(coins, " + remaining+")" );
            //System.out.println(recursionLevel);
            int ways = makeChange(coins, remaining, recursionLevel + 1);
            System.out.println(indentation + "got " + ways + " ways");

            numSubProblemWays += ways;
        }
        return numSubProblemWays;
    }
    // Driver program to test above function
    public static void main(String[] args)
    {
        int arr[] = {1, 3};
        System.out.println(makeChange(arr, 4, 0));
    }
}