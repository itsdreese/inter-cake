import java.util*;



private Map <Integer, Integer> memo = new HashMap<>();


public static int fib(int n){

        if ( n < 0){

             throw new IllegalArgumentException("Indes is negative doesn't work");

        } else if (n == 0 || n == 1){
             return n;
        }


         int prevPrev = 0;
         int prev = 1;
         int current = 0;


         for(int i =1; i< n; i++){

        // Iteration 1: current = 2nd fibonacci
        // Iteration 2: current = 3rd fibonacci
        // Iteration 3: current = 4th fibonacci
        // To get nth fibonacci ... do n-1 iterations.
             current = prev + prevPrev;
             prevPrev = prev;
             prev = current;
         }



        return current;
        }