import java.util.*;

class ChangePossibilities {

    public static int makeChange(int[] coins, int money) {

        //[5 , 10, 25]

        if (money == 0) {
            return 1;
        }


        if (money < 0) {
            return 0;
        }

        int numSubProblemWay = 0;
        for (int i = 0; i < coins.length; i++) {
            int remaining = money - coins[i];
            numSubProblemWays += makeChange(coins, remaining);

        }


        return numSubProblemWays;


        public static void main (String args[]){

            int arr[] = {5, 10, 25};
            makeChange(arr, 30);


        }

    }

}
