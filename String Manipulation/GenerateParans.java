class Solution {
    public List<String> generateParenthesis(int numPairs) {

        List<String> result = new ArrayList<>();
        directedGenerateBalancedParentheses(numPairs, numPairs, "", result); // kick off the recursion
        return result;
    }

    private static void directedGenerateBalancedParentheses(int numLeftParensNeeded , int numRightParensNeeded,
                                                            String parenStringInProgress, List<String> result) {


        if (numLeftParensNeeded == 0 && numRightParensNeeded == 0) {
            result.add(parenStringInProgress);
            return;
        }


  /*
    At each frame of the recursion we have 2 things we can do:

    1.) Insert a left parenthesis
    2.) Insert a right parenthesis

    These represent all of the possibilities of paths we can take from this
    respective call. The path that we can take all depends on the state coming
    into this call.
  */

  /*
    Can we insert a left parenthesis? Only if we have lefts remaining to insert
    at this point in the recursion
  */


        if (numLeftParensNeeded > 0) {

                /*
      numLeftParensNeeded - 1 ->       We are using a left paren
      numRightParensNeeded ->          We did not use a right paren
      parenStringInProgress + "(" ->   We append a left paren to the string in progress
      result ->                        Just pass the result list along for the next call to use
    */

            directedGenerateBalancedParentheses(numLeftParensNeeded - 1, numRightParensNeeded,
                    parenStringInProgress + "(", result);
        }

        if (numLeftParensNeeded < numRightParensNeeded) {

                /*
      numLeftParensNeeded ->           We did not use a left paren
      numRightParensNeeded - 1 ->      We used a right paren
      parenStringInProgress + ")" ->   We append a right paren to the string in progress
      result ->                        Just pass the result list along for the next call to use
    */

            directedGenerateBalancedParentheses(numLeftParensNeeded, numRightParensNeeded - 1,
                    parenStringInProgress + ")", result);
        }

    }

}