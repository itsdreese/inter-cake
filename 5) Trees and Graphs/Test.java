// Java program for different tree traversals

/* Class containing left and right child of current
node and key value*/

public class Test  {
    // Driver method
      static class Node {
        int key;
        Node left, right;

        public Node(int item)
        {
            key = item;
            left = right = null;
        }
    }

     static class BinaryTree {
        // Root of Binary Tree
        Node root;

        BinaryTree()
        {
            root = null;
        }

        void printInorderA(Node node)
        {
            if (node == null)
                return;

            printInorderB(node.left);
            System.out.println("Inside version A with node " + node.key + "\n");
            printInorderB(node.right);
        }

        void printInorderB(Node node)
        {
            if (node == null)
                return;

            printInorderC(node.left);
            System.out.println("Inside version B with node " + node.key + "\n");
            printInorderC(node.right);
        }

        void printInorderC(Node node)
        {
            if (node == null)
                return;

            printInorderD(node.left);
            System.out.println("Inside version C with node " + node.key + "\n");
            printInorderD(node.right);


        }

        void printInorderD(Node node)
        {

            if (node == null)
                return;

            printInorderE(node.left);
            System.out.print(node.key + " ");
            printInorderE(node.right);


        }

        void printInorderE(Node node)
        {
            if (node == null)
                return;

            printInorderF(node.left);
            System.out.print(node.key + " ");
            printInorderF(node.right);
        }

        void printInorderF(Node node)
        {
            if (node == null)
                return;

            System.out.println("Inside version F with node " + node.key);

            printInorderG(node.left);
            System.out.print(node.key + " ");
            printInorderG(node.right);


        }

        void printInorderG(Node node)
        {
            throw new RuntimeException("Shouldn't need to go this deep!");
        }

        // Wrappers over above recursive functions
        void printInorder() {
          printInorderA(root);
          //printInorder(root);
        }

     }
    public static void main(String[] args) {

        BinaryTree tree = new BinaryTree();
        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);

        System.out.println("\nInorder traversal of binary tree is ");
        tree.printInorder();
    }
}