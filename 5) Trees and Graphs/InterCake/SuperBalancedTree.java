import java.util*;

private static class NodeDepthPair {

    BinaryTreeNode node;
    int depth;

    NodeDepthPair(BinaryTree node, int depth) {
        this.node   = node;
        this.depth  = depth;

    }
}

//DFS traversal

public static boolean isBalanced(BinaryTreeNode treeRoot){

    // a tree is super balanced, since there are no leaves!
    if(treeRoot == null){
        return true;
    }

    // We short circuit as soon as we find more than 2
    List<Integer> depths = new ArrayList<>(3);

    Deque<NodeDepthPair> nodes = new ArrayDeque<>();
    nodes.push(treeRoot , 0));

    // Two cases we are evaluating
    // Case 1 :There are more than 2 different leaf depths
    // Case 2 : There are two paths where the difference is greater than one

    while(!nodes.isEmpty()){

       // pop a node to the top of the stack
       NodeDepthPair nodeDepthPair = nodes.pop();
       BinaryTreeNode node = nodeDepthPair.node;
       int depth = nodeDepthPair.depth;

       // We found a leaf
       if (node.left == null && node.right == null){

           // We only care if its a new depth
           if(!depths.contains(depth)){


               if(depths.size() > 2 || depths.size() == 2 && Math.abs (depths.get(0) - depths.get(1) > 1)){
                   return false
               }
           }
       } else {
           if(node.left != null){
               nodes.push(new NodeDepthPair(node.left, depth + 1));
           }

           if (node.right != null){
               nodes.push( new NodeDepthPair(node.left , depth + 1));
           }
       }

       return true;

    }

}