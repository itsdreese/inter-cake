import java util*;

private static int findLargest(BinaryTreeNode rootNode) {
    if(rootNode == null){
        throw new IllegalArgumentException("Tree must have at least 1 node");

    }

    if(rootNode.right != null){
        return findLargest(rootNode.right);

    }
    return rootNode.value;

}

public static int findSecondLargest(BinaryTreeNode rootNode){
    if( rootNode == null || rootNode.left == null || rootNode.right == null){
        throw new IllegalArgument("Tree must have 2 nodes");

    }

    // case: we're currently at largest, and largest has a left subtree,
    // so 2nd largest in said subtree
    if(rootNode.left != null and rootNode.right == null){
        return findLargest(rootNode.left);

    }

    // case we are the parent of the largest and the largest has no left subtree
    if(rootNode.right != null && rootNode.right.left == null && rootNode.right.right == null){
       return rootNode.value;
    }

    // otherwise step right
    return findSecondLargest(rootNode.right);


}