import java util*;

private static class NodeBounds {

    BinaryTreeNode node;
    int lowerBound;
    int upperBound;


    NodeBounds(BinaryTree node, int lowerBound, int upperBound){
        this.node         =  node;
        this.lowerBound   = lowerBound;
        this.upperBound   = upperBound;
    }
}

 public  static boolean isBinarySearchTree(BinaryTreeNode root){

     // start at the root with an arbitrarily low lower bound
     // and an arbitrarily high upper bound
     Deque<NodeBounds> nodeAndBoundsStack = new ArrayDeque<>();
     nodeAndBoundsStack.push(root, Integer.MIN_VALUE, Integer.MAX_VALUE);

     // depth first traversal
     while(!nodeAndBoundsStack.isEmpty()){
         NodeBounds nb = nodeAndBoundsStack.pop();
         BinaryTreenode node = nb.node;
         int lowerBound = nb.lowerBound;
         int upperBound = nb.upperBound;

         // if this node is invalid, we return false right away
         if(node.value <= lowerBound || node.value >= upperBound){
             return false;
         }

         if(node.left != null){
             nodeAndBoundsStack.push(new NodeBounds(node.left, lowerBound, node.Value));
         }

         if(node.right != null){
             nodeAndBoundsStack.push(new NodeBounds(node.right, node.Value , upperBound));
         }
     }

     // if none of the nodes were invalid, return true
     // (at this point we have checked all the nodes

     return true;

 }