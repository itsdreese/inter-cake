import java.util.*;


public static void colorGraph(GraphNode[] graph, String[] colors){


      for(GraphnNode node : graph){
          Set<GraphNode> neighbors = node.getNeighbors();

          if(neighbors.contains(node)){
              throw new IllegalArgumentException(String.format("Legal coloring impossible for node with loop: %s", node.getLabel()));
          }

          // get the node's neigbors colors, as a set so we
          // can check if a color is illegal constant time
          Set<String> illegalColors = new HashSet<>();
          for (GraphNode neighbor : neighbors){
              if(neighbor.hasColor()){
                  illegalColors.add(neighbor.getColor());

              }

          }

          // assign the first legal color
          for (String colors :  color){
              if(!illegalColors.contiains(color)){
                  node.setColor(color);
                  break;
             }

          }


        }





}