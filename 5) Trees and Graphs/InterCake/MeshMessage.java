import java.util*;

public static void bfs(Map<String, String[]> graph, String startNode, String endNode){

    if(!graph.containsKey(startNode)){
       throw new IllegalArgumentException("Start node not in graph");
    }


    if(!graph.containsKey(endNode)){
        throw new IllegalArgumentException("End node not in graph");
    }

    Queue<String> nodesToVisit = new ArrayDeque<>();
    nodesToVisit.add(startNode);


    // keep track of what nodes we've already seen
    // so we don't have to process them twice

    Set<String> nodesAlreadySeen = new HashSet<String>();
    nodesToVisit.add(startNode);

    // keep track of how we got to each node
    // we'll use this to reconstruct the shortest path at the end
    Map<String, String> howWeReachedNodes = new HashMap<>();
    howWeReachedNodes.put(startNode, null);

    while(!nodesToVisit.isEmpty()){
        String currentNode = nodeToVisit.remove();

        // stop when we reach the node

        if(currentNode.equals(endNode)){
            // found it
             return reconstructPath(howWeReachedNopdes, startNode, endNode);
        }


        for (String neighbor: graph.get(currentNode)){
            if(!nodesAlreadySeen.containsKey(neigbor)) {
                nodesToVisit.add(neighbor);
                howWeReachedNodes.put(neighbor, currentNode);
            }
        }
    }




    return null;
}


public static String[] reconstructPath(Map<String String> howWeReachedNodes, String startNode, String endNode){

    List<String> shortestPath = new ArrayList<>();

    //Start from the end path and work backward

    String currentNode = endNode;

    while(currentNode != null){
        shortestPath.add(currentNode);
        currentNode = howWeReachedNode.get(currentNode);

    }

    Collections.reverse(reversedShortestPath);

    return shortestPath.toArray(new String[shortestPath.size()]);


}

//        Map<String, String[]> network = new HashMap<String, String[]>() {{
//        put("Min",     new String[] { "William", "Jayden", "Omar" });
//        put("William", new String[] { "Min", "Noam" });
//        put("Jayden",  new String[] { "Min", "Amelia", "Ren", "Noam" });
//        put("Ren",     new String[] { "Jayden", "Omar" });
//        put("Amelia",  new String[] { "Jayden", "Adam", "Miguel" });
//        put("Adam",    new String[] { "Amelia", "Miguel", "Sofia", "Lucas" });
//        put("Miguel",  new String[] { "Amelia", "Adam", "Liam", "Nathan" });
//        put("Noam",    new String[] { "Nathan", "Jayden", "William" });
//        put("Omar",    new String[] { "Ren", "Min", "Scott" });
//        ...
//        }};


//        {
//        { "Min",     null },
//        { "Jayden",  "Min" },
//        { "Ren",     "Jayden" },
//        { "Amelia",  "Jayden" },
//        { "Adam",    "Amelia" },
//        { "Miguel",  "Amelia" },
//        { "William", "Min" }
//        }