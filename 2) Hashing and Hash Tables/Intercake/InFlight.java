import java.util.*;

public class InFlight{

  public boolean movieRuntime(int flightLength, int[]movieLengths){

    // movie lengths we've seen so far
    Set<Integer> movieLengthsSeen = new HashSet<>();

    for(int firstMovieLength: movieLengths){

      int matchingSecondMovieLength = flightLength - firstMovieLength;
      if(movieLengthsSeen.contains(matchingSecondMovieLength)){
        return true;
      }

      movieLengthsSeen.add(firstMovieLength);
    }

  // we never found a match, so return false
   return false;
  }
  public static void main(String args[]){

  }
}
