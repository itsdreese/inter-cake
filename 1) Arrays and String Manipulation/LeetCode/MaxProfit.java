public class maxProfit{
    public int maxPorfit(int[] prices){
        int minprice = Integer.MAX_VALUE;
        int maxprofit;
        for (int i = 0; i < prices.length; i++){
            if(prices[i] < minprice){
                minprice = prices[i];
            } else if (prices[i] - minprice > maxprofit){
                maxprofit = prices[i] - minprice;
            }
        }

        return maxprofit;
    }
}