import java.util.*;

public class MaxSubArray{

    public int maxSubArray(int[] nums){
        if(nums.length == 1){
            return nums[0];
        }

        int prevSum = nums[0];
        int max = nums[0];

        for(int i = 1; i < nums.length; i++){
            prevSum = nums[i] + (prevSum <= 0 ? 0 : prevSum);
            max = prevSum > max ? prevSum : max;
        }

        return max;

    }


}