import java.util.*;

class Solution {
    public int numSquares(int n) {
        // dp[0] = 0
        // dp[1] = dp[0]+1 = 1
        // dp[2] = dp[1]+1 = 2
        // dp[3] = dp[2]+1 = 3
        // dp[4] = Min{ dp[4-1*1]+1, dp[4-2*2]+1 }
        //       = Min{ dp[3]+1, dp[0]+1 }

        int max = (int) Math.sqrt(n);
        int[] dp = new int[n +1];

        Arrays.fill(dp, Integer.MAX_VALUE);

        for(int i = 1; i <= n; i++){
            for(int j = 1; j <= max; j++)
                if(i == j *j){
                    dp[i]= 1;
                } else if (i > j*j){
                    dp[i] = Math.min(dp[i], dp[i - j*j] + 1);
                }
        }


        return dp[n];
    }


}