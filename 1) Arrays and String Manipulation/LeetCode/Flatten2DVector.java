class Vector2D {
    int x = 0;
    int y = 0;
    int[][] v;
    int next = 0;

    public Vector2D(int[][] v) {
        this.v = v;
    }

    public int next() {
        if (getNext())
        {
            y++;
        }
        return next;
    }

    public boolean hasNext() {
        if (getNext())
            return true;
        return false;
    }

    public boolean getNext() {
        while (x < v.length) {
            int[] row = v[x];
            if (row.length == 0)
            {
                // there are no elements in this row
                x++;
                continue;
            }

            if (y >= row.length) {
                // you have reached the ed of the current col
                y = 0;
                x++;
                continue;
            }
            next = row[y];
            return true; // there a next value
        }
        return false;
    }
}

/**
 * Your Vector2D object will be instantiated and called as such:
 * Vector2D obj = new Vector2D(v);
 * int param_1 = obj.next();
 * boolean param_2 = obj.hasNext();
 */