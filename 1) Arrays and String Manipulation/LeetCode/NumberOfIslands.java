class Solution {
    public int numIslands(char[][] grid) {
        //Do some error checking before diving in
        if(grid == null || grid.length == 0){
            return 0;
        }

        int numIslands = 0;
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j < grid[i].length; j++){
                if(grid[i][j] == '1'){
                    numIslands = numIslands + dfs(grid, i, j);
                }
            }
        }

        return numIslands;

    }

    public int dfs(char[][] grid, int i, int j){
        // Do some error checking
        if(i < 0 || i >= grid.length || j < 0 || j >= grid[0].length || grid[i][j] == '0') {
            return 0;
        }

        grid[i][j] = '0';
        dfs(grid, i + 1, j); // Down
        dfs(grid, i -1 , j); // Up

        dfs(grid, i, j + 1);  // Neighbor to right
        dfs(grid, i, j - 1);  // Neighbor to left

        return 1;

    }
}