import java.util.*;

public class FindSimilarElements {
    public static List findSimilarElements(List<Integer> firstList, List<Integer> secondList) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        int i = 0;
        int j = 0;

        while (i < firstList.size() && j < secondList.size()) {

            if (firstList.get(i) < secondList.get(j)) {
                i++;
            } else if (secondList.get(i) < firstList.get(j)) {
                j++;

            } else if (firstList.get(i) == secondList.get(j)) {
                result.add(firstList.get(i));
                while (firstList.get(i) == secondList.get(j)) {
                    i++;
                    j++;
                }
            }
        }

        return result;



    }


    public static void main(String args[]) {
        List<Integer> first = Arrays.asList(1 ,2, 2, 2,2,2, 3);
        List<Integer> second = Arrays.asList(2, 4, 6);
        List<Integer> result = findSimilarElements(first ,second);

        for (Integer o : result){
            System.out.println(o);
        }




    }

}