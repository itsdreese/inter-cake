import java.util.*;

  public class Meeting {

    private int startTime;
    private int endTime;

    public Meeting(int startTime, int endTime) {
        // number of 30 min blocks past 9:00 am
        this.startTime = startTime;
        this.endTime   = endTime;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }


    public static ArrayList<Integer> mergeRanges(List<Meeting> meetings){

      // Making a copy for comparison -IK
      List<Meeting> sortedMeetings = new ArrayList<>();
      for(Meeting meeting: meetings) {
         Meeting meetingCopy = new Meeting(meeting.getStartTime(), meeting.getEndTime());
         sortedMeetings.add(meetingCopy);
      }



      // sort by start time
      Collections.sort(sortedMeetings, new Comparator<Meeting>(){
        @Override
        public int compare(Meeting m1, Meeting m2) {
           return m1.getStartTime() - m2.getStartTime();
        }
      });

      List<Meeting> mergedMeetings =  new ArrayList<>();
      mergedMeetings.add(sortedMeetings.get(0));

      for (Meeting currentMeeting : sortedMeetings) {

          Meeting lastMergedMeeting = mergedMeetings.get(mergedMeetings.size() - 1);
          // if the current meeting overlaps with the last merged meeting,
          if (currentMeeting.getStartTime() <= lastMergedMeeting.getEndTime()) {
            lastMergedMeeting.setEndTime(Math.max(lastMergedMeeting.getEndTime(), currentMeeting.getEndTime()));

          } else {
            mergedMeetings.add(currentMeeting);
          }


      }

      return mergedMeetings;

      }

      // System.out.println("Printing start times!");
      // printArrayList(startTimes);
      //
      // System.out.println("Printing end times!");
      // printArrayList(endTimes);
      //
      // System.out.println("Printing ranges!");
      // printArrayList(ranges);

      return ranges;

    }

    public static void printArrayList(ArrayList<Integer> i) {
      for (Integer value : i) {
        System.out.println("Value = " + value);
      }

    }

    public static void main (String[] args){
       Meeting one = new Meeting(0,1);
       Meeting two = new Meeting(3,5);
       Meeting three = new Meeting(4,8);
       //Meeting five = new Meeting(9,10);
       Meeting four = new Meeting(10,12);
        Meeting five = new Meeting(9,10);


       List<Meeting> sList = new ArrayList<Meeting>();
       sList.add(three);
       // ArrayList<Integer> eList = new ArrayList<Integer>();
       //
       // Collections.addAll(sList, one.startTime, two.startTime ,
       // three.startTime , four.startTime, five.startTime);
       //
       // Collections.addAll(eList, one.endTime, two.endTime ,
       // three.endTime ,four.endTime, five.endTime);
       //meetingList.add(one.startTime);

       //mergeRanges(sList, eList);

       //mergeRanges((one,two,three,four,five).toArray());

      //System.out.println("Whats up dude");
    }
}
