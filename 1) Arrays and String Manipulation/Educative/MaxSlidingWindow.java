import java.util.*;

public class MaxSlidingWindow{
    public static ArrayDeque<Integer> findMaxSlidingWindow(int[] arr, int windowSize){

      ArrayDeque<Integer> result = new ArrayDeque<>(); // ArrayDeque for storing values
      Deque<Integer> list = new LinkedList<Integer>();  // Creating linked list

      if(arr.length < windowSize){
          return result;
      }

      int startingIndex = 0;

      for(int i = startingIndex; i < windowSize; i++){
          //Removing the last smallest element index
          while(!list.isEmpty() && arr[i] >= arr[list.peekLast()]) {
              list.removeLast();
          }

          // Adding newly picked element index
          list.addLast(i);

          startingIndex = i + 1;
      }

      for(int i = startingIndex; i < arr.length; i++){
          result.add(arr[list.peek()]);

          // Removing all the elements indexes which are not in the current window
          while((!list.isEmpty()) && list.peek()  <= i-windowSize ){
              list.removeFirst();
          }

         // Removing elements indexes which are not required
         while((!list.isEmpty())   && arr[i] >= arr[list.peekLast()]){
             listremoveLast();
         }


         // Adding  newly picked element index
         list.addLast(i);
      }


         result.add(arr[list.peek()]);


      return  result;

    }

    public static void main(String[] args) {
//        int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        System.out.print("Array = ");
        System.out.println(Arrays.toString(array));

        System.out.println("Max = " + findMaxSlidingWindow(array, 3));

        //int[] array2 = { 10, 6, 9, -3, 23, -1, 34, 56, 67, -1, -4, -8, -2, 9, 10, 34, 67 };
        //System.out.println("Array = " + Arrays.toString(array2));

        //System.out.println("Max = " + findMaxSlidingWindow(array2, 3));



    }

}