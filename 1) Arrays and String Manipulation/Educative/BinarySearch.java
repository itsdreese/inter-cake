class BinarySearch{

    static int binSearch(int[] a, int key) {

        int left = 0;
        int right = a.length - 1;


        while (left <= right ){

            int middle = left + ((right - left) / 2);

            if(a[middle] == key){
                return key;
            } else if ( key < a[middle]){
                right = middle - 1;
            } else {
                left = middle + 1;
            }

        }
        return -1;
    }

    public static void main(String []args){
        int[] arr1 = {1,2,4,7,8,12,15,19,24,50,69,80,100};
        System.out.println("Key(12) found at: "+binSearch(arr1,12));
        System.out.println("Key(44) found at: "+binSearch(arr1,44));
        System.out.println("Key(80) found at: "+binSearch(arr1,80));
    }

}