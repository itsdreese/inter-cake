import java.util*;


public static int getMaxProfit(int[] stockPrices){

    if(stockPrices.length < 2)
        throw new IllegalArgumentException("Getting a profit requires at least 2 prices")

    }

    int minPrice = stockPrices[0];
    int maxProfit = stockPrices[1] - stockPrices[0];

    for (int i= 1; i < stockPrices.length; i++)
        int currentPrice = stockPrices[i];

        // see what our potential profit would be if we bought at the
        // min price and sold at the current price
        int potentialProfit = currentPrice - minPrice;

        // update maxProfit if we can do better
        maxProfit = Mathmax (maxProfit, potentialProfit);

        minPrice = Math.min(minPrice, currentPrice);



    }

    return maxProfit;



}