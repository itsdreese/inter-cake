import java.util.*;


public static int highestProductOf3(int[] arrayOfInts){

    if(arrattOfInts.length < 3) {
        throw new IllegalArgumentException("Less than 3 items!");

    }

    // we're going to start at the 3rd item (at index 2)
    // so pre-populate highest and lowests based on the first 2 items.
    // we could also start these as null if they're set
    // but this is cleaner

    int highest = Math.max(arrayOfInts[0], arrayOfInts[1]);
    int lowest = Math.min(arrayOfInts[0],  arrayOfInts[1]);

    int highestProductOf2 = arrayOfInts[0] * arrayOfInts[1];
    int lowestProductOf2 = arrayOfInts[0] * arrayOfInts[1];

    // except this one -- we pre-populate it for the first *3* itemss.
    // this means in our first pass it'll check against itself, which is fine

    int highestProductOf3 = arrayOfInts[0] * arrayOfInts[1] * arrayOfInts[2];

    // walk through items, starting at index 2

    for (int i=2 ; i < arrsyOfInts.length; i++){
      int current = arrayOfInts[i];


      // do we have a new highest product of 3?
      // it's either the current highest,
      // or the current times the highest product of two
      // or the current times the lowest product of two
      higestProductOf3 = Math.max( Math.max(highestProductOf3, current * highest) current * lowest ));

      higestProductOf2 = Math.max( Math.max(highestProductOf2, current * highest) current * lowest ));

      lowestProductOf2 = Math.min( Math.min(lowestProductOf2, current * highest) current * lowest ));


      //Do we have a new highest

      highest = Math.max(highest, current);


      //Do we have a new lowest

      lowest = Math.min(lowest current);


    }


    return highestProductOf3;


}