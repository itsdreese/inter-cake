import java.util*;



public static int[] getProductsOfAllIntsExceptIndex(int[] intArray){

    if(intArray.length < 2) {
       throw new IllegalArgumentException("Need at least three numbers!");

    }



    // we make an array with the length of the input arrsy to
    // hold our products
    int[] productsOfAllIntsExceptAtIndex = new int[intArray.length];

    // for each integer, we find the product of all the integers
    // before it, storing the total prodcut so far each time
    int productSoFar = 1;
    for(int i =0; i < intArray.length; i++){
        productsOfAllIntsExceptAtIndex[i] = productSoFar;
        productSoFar = productSoFar * intArray[i];

    }

    // for each integer, we find the product of all the integer
    //  after it. since each index in products already has the
    // product of all the integers before it, now we're storing
    // the total product of all other integers

    productSoFar = 1;
    for(int i = intArray.length - 1; i >=0; i--){
        productOfAllIntsExceptIndex[i] *= productSoFar;
        productSoFar *= intArray[i];

    }

    return productsOfAllIntsExceptAtIndex;



}