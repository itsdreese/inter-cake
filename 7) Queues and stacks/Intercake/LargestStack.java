import java.util.ArrayDeque;
import java.util.Deque;


public class MaxStack {

    Deque<Integer> stack = new ArrayDeque<>();
    Deque<Integer> maxStack = new ArrayDeque<>();

    public push(int item){
        stack.push(item);

        if (maxStack.isEmpty() || item > maxStack.peek()){
            maxStack.push(item);
        }
    }

    public int pop(){
        int item = stack.pop();
        if(item == maxStack.peek()){
            maxStack.pop();
        }

        return item;
    }

    public int getMax(){
        return maxStack.peek();
    }


}