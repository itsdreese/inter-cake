class Solution {
    public int[] asteroidCollision(int[] asteroids) {

        Stack<Integer> stack = new Stack();
        boolean destroyed;
        for(int ast: asteroids){
            destroyed = false;
            while(!stack.isEmpty() && ast < 0 && 0 < stack.peek()){
                if(stack.peek() < -ast){
                    stack.pop();
                    continue;
                } else if (stack.peek() == -ast){
                    destroyed = true;
                    stack.pop();
                } else {
                    destroyed = true;
                }
                break;
            }

            if(!destroyed){
                stack.push(ast);
            }

        }

        int[] ans = new int[stack.size()];
        for(int t = ans.length -1; t >=0; t--){
            ans[t] = stack.pop();
        }

        return ans;
    }

}