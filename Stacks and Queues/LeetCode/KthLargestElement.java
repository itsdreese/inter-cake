import java util.*;

public int findKthLargestElement(int[] nums , int k){
    PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>();
    for(int i: nums){
        minHeap.add(i);

    }

    return minHeap.remove();
}