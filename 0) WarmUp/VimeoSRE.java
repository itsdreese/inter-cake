import java.util.*;
import java.io.*;
import java.util.regex.*;
import java.text.DecimalFormat;
import java.math.*;

public class VimeoSRE {
    public static void main(String args[]) throws Exception {

        // Handles case where we dont have enough arguments
        if (args.length < 2) {
            System.out.println("ERROR: You have not put in enough variables!");
            System.out.println("Please use exceptable usage: ");
            System.out.println("                              ");
            System.out.print("For ex: java VimeoSRE $epochStartTime");
            System.out.print(" $epochEndTime filename1.txt filename2.txt filename3.txt ");
            System.out.println("                              ");
            System.exit(1);
        }

        //Decided to created Two Hashmaps here.
        //I think it might be easier to follow to distinguish
        //what each value represents. One could easily have just made
        //HashMap<String, List> domain= new HashMap<>(); and store
        // the first value of the list as the domain lineCount
        // and the second value the number of errors

        HashMap<String, Double> domainCountMap = new HashMap<>();
        domainCountMap.put("vimeo.com", 0.00);
        domainCountMap.put("player.vimeo.com", 0.00);
        domainCountMap.put("api.vimeo.com", 0.00);

        HashMap<String, Double> domainErrorMap = new HashMap<>();
        domainErrorMap.put("vimeo.com", 0.00);
        domainErrorMap.put("player.vimeo.com", 0.00);
        domainErrorMap.put("api.vimeo.com", 0.00);

        // I know this is ugly :( Would probably thinking of putting this in some kind
        // of regex -IK
        String s = domainCountMap.keySet().toString().replaceAll(",", " ").replaceAll("\\[", "").replaceAll("\\]", "");
        int lineCount = 0;
        Double startTime = Double.parseDouble(args[0]);
        Double endTime = Double.parseDouble(args[1]);

        for (int i = 2; i < args.length; i++) {
            File currentFile = new File(args[i]);
            try {
                Scanner sc = new Scanner(currentFile);
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    String pipe = "\\|";
                    Pattern pattern = Pattern.compile(pipe);
                    String[] logLine = pattern.split(line);
                    String domainName = logLine[2].replaceAll(" ", "");
                    String httpStatus = logLine[4].replaceAll(" ", "");
                    Double logStartTime = Double.parseDouble(logLine[0]);

                    if ((s.contains(domainName)) && ((startTime.compareTo(logStartTime) <= 0) && (endTime.compareTo(logStartTime) > 0))) {
                        double domainCount = 1.00 + domainCountMap.get(domainName);
                        domainCountMap.put(domainName, domainCount);

                        if (httpStatus.matches("^5..")) {
                            double domainErrorCount = 1.00 + domainErrorMap.get(domainName);
                            domainErrorMap.put(domainName, domainErrorCount);
                        }
                    }
                }
                sc.close();
            } catch (Exception e) {
                System.out.println("ERROR: Please provide path to a valid file. Stacktrace below :");
                System.out.println("");
                e.printStackTrace();
            }

        }
        System.out.println("Between the time" + " " + startTime + " " + "and time" + " " + endTime + " : ");
        System.out.println("                                                           ");
        Set domainCountSet = domainCountMap.entrySet();
        Set domainErrorSet = domainErrorMap.entrySet();

        Iterator domainCountIterator = domainCountSet.iterator();
        Iterator domainErrorIterator = domainErrorSet.iterator();

        while (domainCountIterator.hasNext() && domainErrorIterator.hasNext()) {
            Map.Entry dci = (Map.Entry) domainCountIterator.next();
            Map.Entry dce = (Map.Entry) domainErrorIterator.next();

            DecimalFormat df = new DecimalFormat("#00.00"); // Set your desired format here.

            if ((double) dci.getValue() == 0) {
                System.out.println("INFO: No log lines for" + " " + dci.getKey());
                System.out.println("                                       ");
            } else {
                double domainErrorPercent = (double) dce.getValue() / (double) dci.getValue();
                System.out.print(dce.getKey() + " returned ");
                System.out.println(df.format(domainErrorPercent * 100) + "" + "% 5xx errors");
            }
        }
        System.out.println();
    }
}
