// Vimeo SRE solution


Please write a README file including the following information:

1) Sample Invocation : java VimeoSRE START_TIME FINISH_TIME  filename1.txt filename2.txt
2) Some external dependencies include Java util, java regex, decimal format package. If you have java installed
   you shouldn't really need to download or install anything honestly.
3) Some assumptions I've made
    a. User will be giving me a valid log file.
    b. User will pass in files via command line.
    c. There are only a fixed amount of domains(vimeo.com, player.vimeo.com ,api.vimeo.com)
    d. Percentage is calculated against individual domains for example if there are 5 vimeo.com api calls
       and 1 of them have 5xx errors the percentage will be 1/5 or 20%


Example log file:

1493969101.638 | https | player.vimeo.com | POST | 200 | 149399101591904,149396911639670, | IAD |  | 10.10.3.53 | 0.006
1493969101.639 | https | player.vimeo.com | GET  | 500 | 149396101592060,149396910168753, | IAD |  | 10.13.4.52 | 0.007
1493969101.638 | https | vimeo.com        | POST | 200 | 149396910159904,149396910163967, | IAD |  | 10.10.3.52 | 0.006
1493969101.639 | https | vimeo.com        | GET  | 200 | 149396101592060,149396910168753, | IAD |  | 10.13.4.52 | 0.007
1493969101.639 | https | vimeo.com        | GET  | 200 | 149396101592060,149396910168753, | IAD |  | 10.13.4.52 | 0.007
1493969101.639 | https | vimeo.com        | GET  | 200 | 149396101592060,149396910168753, | IAD |  | 10.13.4.52 | 0.007
1493969101.639 | https | vimeo.com        | GET  | 500 | 149396101592060,149396910168753, | IAD |  | 10.13.4.52 | 0.007


Example output:

$ java VimeoSRE 1593969101.638 999999999.99 log_test.txt
Between the time 1.593969101638E9 and time 9.9999999999E8 :

INFO: No log lines for api.vimeo.com

player.vimeo.com returned 00.50% 5xx errors
vimeo.com returned 00.20% 5xx errors
