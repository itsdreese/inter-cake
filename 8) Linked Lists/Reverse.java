public class Reverse {

    public static LinkedListNode reverse_iterative(LinkedListNode head){
        // no need to reverse if head is null or there is only 1 node

        if(head == null || head.next == null){
            return head;
        }

        LinkedListNode list_to_do = head.next;
        LinkedListNode reversed_list = head;

        reversed_list.next = null;

        while( list_to_do != null ){
            LinkedListNode temp = list_to_do;
            list_to_do = list_to_do.next;

            temp.next = reversed_list;
            reversed_list = temp;

        }

        return reversed_list;
    }
}